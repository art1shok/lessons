`use strict`
// old
// const gulp = require("gulp");
// const sass = require("gulp-sass");

import gulp from "gulp";
import gulpSass from "gulp-sass";
import dartSass from "sass";
import browsersync from "browser-sync";
import CleanCSS from "gulp-clean-css";
import rename from "gulp-rename";

const sass = gulpSass(dartSass);

console.log(gulp);

const buildCss = () => {
  return gulp
    .src("./src/scss/main.scss")
    .pipe(sass({ outputStyle: "expanded"}))
    .pipe(CleanCSS({compatibility: 'ie8'}))
    .pipe(rename({
      suffix: ".min",
      extname: ".css"
    }))
    .pipe(gulp.dest("./build/css/"))
    .pipe(browsersync.stream());
};

const buildHtml = () => {
  return gulp
    .src("./src/*.html")
    .pipe(gulp.dest("./build"))
    .pipe(browsersync.stream())
}

const watchCss = () => {
  gulp.watch("./src/scss/**/*", buildCss)
};

const watchHTML = () => {
  gulp.watch("./src/*.html", buildHtml)
};

gulp.task("default", gulp.parallel(buildCss, watchCss, watchHTML, buildHtml));

//
// const scss = () => {
//   return gulp
//     .src("./src/scss/main.scss")
//     .pipe(sass({ outputStyle: "expanded"}))
//     .pipe(gulp.dest("./build/css/"))
// }

//Дописать gulpfile js, что бы все js фалй
