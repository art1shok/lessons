// const chooseColor = (color) => {
//   const circles = document.querySelectorAll(".circle");
//
//   circles.forEach((circle) => {
//     console.log(circle.innerText)
//     if(circle.innerText === color) {
//       circle.style.cssText =`
//         background-color: ${color};
//       `
//     }
//   })
// }

// chooseColor("red");
// chooseColor("green");
// chooseColor("yellow");

// console.log(document.getElementById("svetofor").children);
//
// const chooseColor = (color) => {
//   const circles = document.querySelectorAll(".circle");
//
//   circles.forEach((circle) => {
//
//     // console.log(circle.classList.value.includes(color));
//     console.log(circle.className.includes(color));
//     if(!circle.className.includes(color)) {
//       circle.style.cssText =`
//         background-color: grey;
//       `
//     }
//   })
// }
// // chooseColor("red");
//
// chooseColor("green");

const svetofor = document.getElementById('svetofor');

//Глобальные события
//DOMContentLoaded запускается когдаваш документ (страница) загрузился, будут доступны все ваши элементы,но не будет изображенй и таблиц стилей.
// document.addEventListener("DOMContentLoaded", function(event) {
//   console.log("DOM fully loaded and parsed");
//   // console.log(event)
// });

// console.log(window);
// console.log(document);
// //window.onload -> Когда уже всё загрузилось включая изображения и таблицы стилей
// window.onload = function () {
//   console.log("Your window is fully loaded and parsed");
// }

//window.onbeforeunload -> Если мы в этом событии возвращаем false, то браузер запросит потверждение выхода со страницы.
// window.onbeforeunload = function () {
//   console.log("onbeforeunload")
//   return false;
// }

//window.onunload -> Событие которое происходит когда пользователь покидает страницу
// window.onunload = function () {
//   console.log("onunload")
// }

//
// const onMouseOver = () => {
//   console.log(111)
// }
//
// const onMouseOver2 = (event) => {
//   console.log(event)
// }

//функции которые начинаются с on(название события) являются обработчиками событий
// svetofor.children[2].addEventListener("click", (event) => {
//   console.log(222)
//   console.log("event children", event)
//   console.log("event type", event.type)//Тип произошедшего события
//   console.log("event target", event.target)//Это элемент на котором произошло событие
// });

// svetofor.addEventListener("click", (event) => {
//   console.log("event svetofor", event);
//   console.log("event target", event.target);//Это элемент на котором произошло событие то есть это может быть дочерний элемент
//   console.log("event current target", event.currentTarget);//Элемент на которром висит обработчик
//   console.log(event.clientX, event.clientY) //Показывает координаты в пикселях где произошло событие
// });
//
// svetofor.addEventListener("click", (event) => {
//   console.log("event svetofor", event);
//   console.log("event target", event.target);//Это элемент на котором произошло событие то есть это может быть дочерний элемент
//   console.log("event current target", event.currentTarget);//Элемент на которром висит обработчик
//   console.log(event.clientX, event.clientY) //Показывает координаты в пикселях где произошло событие
// }, {
//   once: true,
// });
//События мыши

svetofor.onmouseover = () => { //Происходит когда курсор наводиться на элемент

};

svetofor.onmouseout = () => { //Происходит когда курсор убирается с элемента

};

svetofor.click = () => { //Происходит когда мышью нажимают на элемент

};

svetofor.ondblclick = () => { //Происходит когда мышью нажимают на элемент дважды

};

svetofor.onmousemove = () => { //Происходит когда мышка двигается над элементом

};

svetofor.oncontextmenu = () => { //Вызывает контекстное меню (чаще всего на правую клавишу мыши)

};

/**
 * 1. Написать скрипт, который создаст элемент button с текстом «Войти».
 *
 * При клике по кнопке выводить alert с сообщением: «Добро пожаловать!».
 *
 * 2. Улучшить скрипт из предыдущего задания.
 *
 * При наведении на кнопку  указателем мыши, выводить alert с сообщением:
 * «При клике по кнопке вы войдёте в систему.».
 *
 * Сообщение должно выводиться один раз.

 *
 */

// svetofor.addEventListener("click", (event) => {
// }, {
//   once: true,
//   // capture: true,
//   // passive: true
// });

const button = document.createElement('button');

button.innerText = 'Войти';

document.body.append(button);

button.onclick = () => {
  // alert('Добро пожаловать!');
};

// button.addEventListener('mouseover', () => {
//   //   alert('При клике по кнопке вы войдёте в систему.');
//   // },
//   // {
//   //   once: true
//   // }
// );

button.onclick = () => {
  console.log('Добро пожаловать!');
};

button.click()
button.click()
button.click()
button.click()
button.click()

