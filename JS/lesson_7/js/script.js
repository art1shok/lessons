'use strict';

const someString1 = '123' + '\n' + '123 " \\ \' \" ©  ';
// console.log(someString1);

const someString2 = '\n 123 \n 123 \n \' 123';

// console.log(someString2);

const someString3 = `
   ${someString1}
 + ${someString2}`;

// console.log('third string', someString3);
                  //0123456789
const testString = "some test string width id Text";
// console.log(testString.length); //30
//
// console.log(testString.split(' ')); // ['some', 'test', 'string', 'width', 'id', 'text']
// console.log(testString.split('')); // ['s', 'o', 'm', 'e', ' ', 't', 'e', 's', 't', ' ', 's', 't', 'r', 'i', 'n', 'g', ' ', 'w', 'i', 'd', 't', 'h', ' ', 'i', 'd', ' ', 't', 'e', 'x', 't']
      //                                    0   1    2     3   4     5  6    7     8    9    10   11  12   13
// const split = (separator) => {
//    let newString;
//
//    return newString;
// }

// console.log(testString[2]); //m
// console.log(testString.charAt(21)); //h
// console.log(testString[testString.length - 1]); //t
//
//
// console.log(testString[200]); //undefined
// console.log(testString.charAt(200)); //""

let newArray = ['some', 'test', 'string', 'width', 'id', 'text'];

newArray[200] = "newText";

// console.log(newArray[7]);
// console.log([]);

for (let char of testString) {
   // console.log(char);//
}

// testString[2] = "O"// was "o"

let newTestString = testString[2] + "O";

// console.log(newTestString);


const lowerCase = "SOme WOrd.tO LoWer Case";
const upperCase = "all words to upper case";

// console.log(lowerCase.toLowerCase().split('.').map((element, idx) => {
//    for (let i = 0; element.split("").length; i++) {
//       element.split("")[0].toUpperCase();
//    }
//    return element;
// }));

//Lower case && Upper Case
// console.log(lowerCase, " lowerCase before");
// console.log(lowerCase.toLowerCase(), " lowerCase after");
// console.log(upperCase, " upperCase before");
// console.log(upperCase.toUpperCase(), " upperCase after");

//Методы поиска внутри строки
//index of && last index of
// console.log(testString.indexOf("ing"));
// console.log(testString.lastIndexOf("ing"));


//includes
// console.log(testString.includes("ing", 11))//true
// console.log(testString.includes("ing", 18))//false

//startsWith && endWith

// console.log(testString.startsWith("test"))//false
// console.log(testString.endsWith("text"))//true

//Creating substrings
//slice
const stringForSlice = "its a new test string";

const newSubstring1 = stringForSlice.slice(10, 14); //test
const newSubstring2 = stringForSlice.substring(stringForSlice.length, 10); //test string
const newSubstring3 = stringForSlice.slice(14, 10); // ""
const newSubstring4 = stringForSlice.substr(14, 10); // " string"


// console.log(newSubstring1);
// console.log(newSubstring2);
// console.log(newSubstring3);
// console.log(newSubstring4);

const codePointString = "Aa";
// console.log(codePointString.codePointAt(0)); //65
// console.log(codePointString.codePointAt(1)); //97


// console.log("abc".localeCompare("")) // 1
// console.log('Österreich' > 'Osterriechring') // true


//Найти нормальный пример сравнивания строк

//Data & time

//1 января 1970 года timestamp

const currentDate = new Date();//Вызываем функцию конструктор Date();

// console.log(currentDate);

Date.now();


const dateFromBackend = "2020-01-01";
// console.log(new Date(dateFromBackend));

const date2 = new Date(2023, 4, 56); //
const date3 = new Date(2023, 0, 1); //
const date4 = new Date(2023, 1, 0); //

console.log(date2);//
console.log(date3);//
console.log(date4);//


