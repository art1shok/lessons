// number string boolean null object symbol bigInt undefined types of data
//
// let string = "1";
// let someNumber = 1;
//
// console.log(string, typeof someNumber);
//
// console.log(string === someNumber);
//
// const user = {
//   name: "Artem",
//   age: 23
// };
//
// console.log("user", user, user.name);
//
// let array = [
//   "Banana",
//   "Apple",
//   "Orange",
// ];
//
// console.log(array);
//
//
// const value = true;
// const secondValue = [];
//
// console.log("value", value);
// console.log("value string", String(value));
// console.log("secondValue", secondValue);
// console.log("secondValue string", String([]));
//
// console.log(Number(NaN)); //Nan
// console.log(+NaN); //Nan
// console.log(+undefined); //Nan
// console.log(Number(false)); //0 (true -> 1)
// console.log(Number("123")); // 123
// console.log(+"123"); //123
// console.log(Number(12)); //12
// console.log(Number("123dawdaw")); //Nan
//
//
// console.log(Boolean(0)); //false
// console.log(Boolean(NaN)); // NaN or undefined => false
// console.log(Boolean(null)); //false
// console.log(Boolean(undefined)); //false
// console.log(Boolean("")); //false
//
// console.log([]); //true
// console.log({}); //true
// console.log("space", Boolean(" ")); //true
// console.log(Boolean("undefined")); //true
// console.log(Boolean("0")); //true
//
// const isTrue = true;
// const isFalse = false;
//
// const someString = "Hello";
// const anotherString = "world";
//
// const thirdString = someString + " " + anotherString;
//
// console.log(thirdString);
//
// const ages = [12, 15, 18]; //array.length = 3;
//
// for (let i = 0; i < ages.length; i++) {
//   console.log(`His age is ${ages[i]}`);
//   console.log(i, "index");
// }
//
// console.log(Number.MAX_VALUE, "max value")

// const FirstNumber = +prompt("Please enter first number");
// const SecondNumber = +prompt("Please enter second number");
// const ThirdNumber = +prompt("Please enter third number");
//
// if(Number.isNaN(FirstNumber) || Number.isNaN(SecondNumber) || Number.isNaN(ThirdNumber)) {
//   console.log("Something went wrong, your number is not a number");
// } else {
//   console.log("sum", FirstNumber + SecondNumber + ThirdNumber);
// }
//
// console.log(true || true); //True
// console.log(false || false); //false
// console.log(false || true); //true
// console.log(true || false); //true
// console.log(0 || '' || 1 || 2); //
//
// if (0 || '' || 1 || 2) {
//   console.log('true 1111');
// }
//
// console.log(true && true); //true
// console.log(false && true); //false
// console.log(false && false && true); //false
// console.log(true && true && false); //false
//
// if (' ' && 1 && 2 && 0) {
//   console.log('&& is working');
// }
//
// console.log(parseInt('001101', 16)); //13
//
// if (' ' && 1 && 2 && 0) {
//   console.log('&& is working');
// } else {
//   console.log(' 1 else is working');
// }
//
// if (' ' && 1 && 2 && 0) {
//   console.log('&& is working');
// } else if ([] && 'daw') {
//   console.log(' 2 new if is working');
// }
//
// const userIsAuth = true;
// const someData = ['apple', 'book'];
//
// if (someData) {
//   for (let i = 0; i < someData.length; i++) {
//     console.log(`Your chose is ${someData[i]}`);
//     console.log(i, 'index');
//   }
// }
//
//
// const userType = "admin";
// const anotherUserType = "user";
//
//   switch ("dawdawd") {
//     case "user":
//       console.log("User type is just a user");
//       break;
//     case "admin":
//       console.log("User type is admin");
//       break;
//     case "super admin":
//       console.log("User type is super admin");
//       break;
//
//     default:
//       console.log("User type is unknown");
//   }
//

 // 'Ключевое слово' (условие) {
//  Тело цикла
// }
// while(условие){}
//do{}while()
//for(
// let i = 0;
// let sum = 0;
// let newArrayForLoop = [1, 2, 4, 51, 21];
// while(i < newArrayForLoop.length) {
//   sum += newArrayForLoop[i];
//   i++;
// }
// console.log(sum);
//
// let newSum = 0;
// let y = 0;
//
// do {
//   newSum += 5;
//   console.log(newSum, "sum");
//   console.log(y);
//   y++;
// } while (y < 0)


let a = 1;

console.log(!a, "false");

console.log(!!a, "true");

const message = "Hello";
const secondMessage = "world!"
console.log(message + secondMessage); //Helloworld!

console.log(`${message} + ${secondMessage}`)//Hello + world!
console.log(`${message} ${secondMessage}`) // Hello world!










