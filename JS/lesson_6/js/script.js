'use strict';

//Object => Тип данных, который позволяет хранить в себе любое колличество других типов даных.
//Данные внутри объекта хранятся парами Ключ: значение.

//Что такое ссылочные типы данных?
//Как сохраняются данные при объявлении объекта?

let user = {
  name: 'Dima',
  age: 34
};

const firstNumber = 1;
// console.log(user, 'user first attempt');

let newUser = user;

newUser.name = 'Sasha';

const newFunction = () => {
  return 100;
};

// console.log(newFunction());
//
// console.log(newUser, 'newUser');
// console.log(user, 'user');

const newUserAge = 18;
const newUserName = 'Ihor';

const userKey = 'age';

// console.log(user['age'], 'user age');

function createUser (userAge, userName) {
  return {};
}

const student = {
  name: 'Ihor',
  age: 18,
  sayHi () {
    console.log(`${this.name} wanna say Hi`);
  },
  happyBirthday () {
    this.age += 1;
    console.log('His age is ' + this.age);
  }
};
//
// console.log(student[userKey], 'userKey');// age = 18;

// student.sayHi();

const firstStudent = {};

Object.defineProperties(firstStudent, {
  firstName: {
    value: 'Kolya',
    writable: false,
  },
  lastName: {
    value: 'Zelenskiy',
    writable: false
  },
  age: {
    value: 22,
    writable: true
  }
});
//
// firstStudent.firstName = 'German';
// firstStudent.lastName = 'German';
// firstStudent.age.writable = false;
// firstStudent.age = 12;
//
// console.log(firstStudent.firstName);
// console.log(firstStudent.lastName);
// console.log(firstStudent.age);

const secondStudent = {
  firstName: 'Mike',
  lastName: 'Jordan',
  age: 55
};

Object.freeze(secondStudent);

// secondStudent.firstName = "Shakil";

// console.log(secondStudent.firstName);
// console.log(secondStudent.lastName);

//1 task
//Создать объект, в котором будет 3 свойства. Все три свойства должны быть числами.
//И этот объект должен иметь метод, который возвращает сумму данных чисел.

const firstTaskObject = {
  firstNumber: 1,
  secondNumber: 2,
  thirdNumber: 3,
  sum () {
    // return firstNumber + secondNumber + thirdNumber; //так не делать
    return this.firstNumber + this.secondNumber + this.thirdNumber;
  }
};

// const secondTaskObject = {
//   firstName: 'Mike',
//   lastName: 'Jordan',
//   age: 55,
//   innerObject: {
//     team: 'Chicago Bulls',
//     secondTeam: 'Dinamo'
//   }
// };
//Нам нужно написать метод, внутри которого мы можем пройти
// по всем ключам данного объекта, и вывести их значение в консоль.
//Так же, пройтись по ключам внутреннего объекта, и тоже вывести значение.

// console.log(firstTaskObject.sum());
// console.log(firstTaskObject.firstNumber);

const secondTaskObject = {
  firstName: "Mike",
  lastName: "Jordan",
  age: 55,
  innerObject: {
    team: "Chicago Bulls",
    secondTeam: "Dynamo",
  },
  count() {
    for (let firstLevelKey in secondTaskObject) {
      if (typeof secondTaskObject[firstLevelKey] === "object") {
        for (let secondLevelKey in secondTaskObject[firstLevelKey]) {
          console.log(`${secondLevelKey}: `, secondTaskObject[firstLevelKey][secondLevelKey]);
        }
      } else {
        if (typeof secondTaskObject[firstLevelKey] !== "function")
          console.log(`${firstLevelKey}: `, secondTaskObject[firstLevelKey]);
      }
    }
  },
};

secondTaskObject.count();

// Пройтись по объекту произвольной глубины.
// Это может быть как 1 объект вложенный в другой,
// так и объект в объекте в объекте в объекте.
// То есть глубина объекта может быть любой.
// Нужно вывести все ключи: значения данного объекта. (Hint: использовать рекурсию)

//  * Написать функцию-помощник для ресторана.
//  *
//  * Функция обладает двумя параметрами:
//  * - Размер заказа (small, medium, large);
//  * - Тип обеда (breakfast, lunch, dinner).
//  *
//  * Функция возвращает объект с двумя полями:
//  * - totalPrice — общая сумма блюда с учётом его размера и типа;
//  * - totalCalories — количество калорий, содержащееся в блюде с учётом его размера и типа.
//  *

const priceList = {
  sizes: {
    small: {
      price: 15,
      calories: 250,
    },
    medium: {
      price: 25,
      calories: 340,
    },
    large: {
      price: 35,
      calories: 440,
    },
  },
  types: {
    breakfast: {
      price: 4,
      calories: 25,
    },
    lunch: {
      price: 5,
      calories: 5,
    },
    dinner: {
      price: 10,
      calories: 50,
    },
  },
};

function calculate (size, type) {
  let totalPrice = 0;
  let totalCalories = 0;

  totalPrice += priceList.sizes[size].price;
  // totalPrice += priceList.sizes.medium.price; //1
  // totalPrice += priceList.sizes.small.price; //2
  totalPrice += priceList.types[type].price;
  // totalPrice += priceList.types.lunch.price; //1
  // totalPrice += priceList.types.breakfast.price; //2

  totalCalories += priceList.sizes[size].calories;
  totalCalories += priceList.types[type].calories;

  return {
    totalPrice,
    totalCalories
  }
}

console.log(calculate("medium", "lunch")); //1
console.log(calculate("small", "breakfast")); //2

