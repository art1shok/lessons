//Глобальные события
//DOMContentLoaded запускается когдаваш документ (страница) загрузился, будут доступны все ваши элементы,но не будет изображенй и таблиц стилей.
// document.addEventListener("DOMContentLoaded", function(event) {
//   console.log("DOM fully loaded and parsed");
//   // console.log(event)
// });

// console.log(window);
// console.log(document);
// //window.onload -> Когда уже всё загрузилось включая изображения и таблицы стилей
// window.onload = function () {
//   console.log("Your window is fully loaded and parsed");
// }

//window.onbeforeunload -> Если мы в этом событии возвращаем false, то браузер запросит потверждение выхода со страницы.
// window.onbeforeunload = function () {
//   console.log("onbeforeunload")
//   return false;
// }

//window.onunload -> Событие которое происходит когда пользователь покидает страницу
// window.onunload = function () {
//   console.log("onunload")
// }


// const onMouseOver = () => {
//   console.log(111)
// }
//
// const onMouseOver2 = (event) => {
//   console.log(event)
// }
// const main = document.querySelector(".main-content");
//
// main.addEventListener("click", (event) => {
//   console.log("main", event);
// })
//
// const button = document.querySelector(".button");
// const button1 = document.querySelector(".button1");
// const buttonContainer = document.querySelector(".button-container");
//
// button.addEventListener("click", (event) => {
//   event.stopPropagation();
//   console.log("button0", event);
//
// })
//
// button1.addEventListener("click", (event) => {
//   console.log("button 1", event);
// })
//
// buttonContainer.addEventListener("click", (event) => {
//   console.log("outer element", event);
//   console.log(event.currentTarget);
// })
//
// const list = document.querySelector(".list");
// const listItems = document.querySelectorAll(".list-item");
// main.addEventListener("click", (event) => {
//   console.log("main", event);
//   console.log("currentElement", event.target);
//   if (+event.target.innerText %2 === 0) {
//     event.target.innerText = "10";
//   }
// });


//
//
// list.addEventListener("click", (event) => {
//   console.log("list", event);
//   console.log(list.children);
//
//
//   if (+event.target.innerText %2 === 0) {
//     event.target.innerText = "10";
//   }
// })
//
//
// const submitButton = document.querySelector(".submit");
//
// submitButton.addEventListener("click", (event) => {
//   event.preventDefault();
//   console.log("click");
// })
//
//
// const form = document.querySelector(".form");
//
// form.addEventListener("submit", (event) => {
//   event.preventDefault();
//   console.log("submit");
// })

// let startTime; // начальное время
//
// document.body.onmousedown = function(e) {
//   // which указывает на клавишу (1 - левая)
//   if (e.which === 1) {
//     console.log('mousedown');
//     startTime = +new Date(); // получаем время в мс при нажатии на клавишу мыши
//   }
// }
//
// document.body.onmouseup = function(e) {
//   if (e.which === 1) {
//     console.log('mouseup');
//     let endTime = +new Date();
//     let time = (endTime - startTime) / 1000; // из мс получаем секунды
//     console.log(time + ' sec');
//   }
// }

//
// const list2 = document.querySelector(".list2");
//
//
// list2.addEventListener("click", (event) => {
//   const item = document.getElementById(`${event.target.innerText}`);
//
//   item.classList.toggle("hidden");
// })
//
// const counterButton = document.querySelector(".counter__button");
//
// let count = 0;
//
// const text = document.getElementById("text");
//
// console.log(text);
//
// counterButton.addEventListener("click", (e) => {
//   console.log(text, "2")
//   count++;
//   text.innerText = `${count}`;
// })


// * При клике на элемент меню добавлять к нему CSS-класс .active.
// * Если такой класс уже существует на другом элементе меню, необходимо
// * с того, предыдущего элемента CSS-класс .active снять.
// *
// * У каждый элемент меню — это ссылка, ведущая на google.
// * С помощью JavaScript отключить переход по всем ссылкам.
// *


const ul = document.querySelector("ul");
const links = document.querySelectorAll("a");

ul.addEventListener("click", (event) => {
  event.preventDefault();

  for (let link of links) {
    if(link.classList.contains("active")){
      link.classList.remove("active");
    }
  }

  event.target.classList.add("active");
})
