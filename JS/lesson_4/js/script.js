// const someFunction = function (number) {
//   number += 1;
//   return number + 1;
// }
//
// const someFunction = (number) => number + 1;
//
// const anotherFunction = function (number) {
//   console.log(arguments, 'arguments');
//
//   return number + 1;
// };
//
// console.log(someFunction(4));

//Функция, которая будет принимать 3 параметра, 1е число, 2е число. Арифметический знак
//И в зависимости от арифметического знака мы будем выполнять 1 из 4 операций: +, -, /, *;
//Если один из параметров был введен неверно - выводить в консоль ошибку.
//Optional:
//Ещё добавить приведение к степени 1 числа ^ 2 числа.
//Добыть корень из числа 1 число - основа. 2 число - степень корня. root
// let number1 = 3;
// let number2 = 5;
// let string = "pow"
//
// const calculate = function (num1, num2, item) {
//   switch (item) {
//     case '+':
//       return num1 + num2;
//     case '-':
//       return num1 - num2;
//     case '*':
//       return num1 * num2;
//     case '/':
//       return num1 * num2;
//     case '**':
//       return num1 ** num2;
//     case "pow":
//       return num1 ** (1/num2);
//     default:
//       console.log('Something went wrong');
//   }
// };
//
// console.log(calculate(number1, number2, string))

//Написать функцию-счетчик count
//Функция принимает два параметра
//Первый — число, с которого необходимо начать счёт
//Второй — число, которым необходимо закончить счёт.

//Если у вас первое число(с которого необходимо начать счёт) больше чем второе (которым необходимо закончить счёт)
// Ошибка, счёт невозможен

//Если у вас оба числа равны
// Ошибка, счёт невозможен

//Если счет начался -> вывести консольку
// Отсчёт начат

// Каждый шаг выводить в консоль

//Отсчёт завершен

// * Функцию-счётчик из предыдущего задания расширить дополнительным функционалом:
//   * - Добавить ей третий параметр, и выводить в консоль только числа, кратные значению из этого параметра;
// * - Генерировать ошибку, если функция была вызвана не с тремя аргументами;
// * - Генерировать ошибку, если любой из аргументов не является допустимым числом.
//

// x = 5;
// window.undefined = 5; //Это read-only свойство в которое нельзя присваивать какое либо значение, но не используя строгий режим - это можно сделать
// console.log(x, 'x without use strict');
//
// const withUseStrict = () => {
//   'use strict';
//   y = 3; //Используя строгий режим нам не разрешается присваивать значение в неопределённую переменную
//
// };

const count = function (first, second, third) {
  if (arguments.length !== 3) {
    console.log('неправильное количество аргументов');
    return;
  }

  for (const argument of arguments) {
    if (typeof argument !== 'number' || isNaN(argument)) {
      console.log('error2');
      return;
    }
  }

  if (first >= second) {
    console.log('счет невозможен');
  } else {
    console.log('отсчет начат');
    for (let i = first; i <= second; i++) {
      // console.log(i);
      if (i % third === 0) {
        console.log(i);
      }
    }

    console.log('отсчет закончен');
  }
};

count(5, 25, 5);

