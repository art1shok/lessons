// const data = 'apple watch';
//
// const anotherData = 'another data';
//

let isTrue = true;
let isFalse = false;
//
// if (!isTrue) {
//   console.log('data', data);
// } else {
//   console.log('anotherData', anotherData);
// }
//
// switch (undefined) {
//   case true: {
//     console.log('true');
//     break;
//   }
//   case false: {
//     console.log('false');
//     break;
//   }
//   case '0': {
//     console.log('string 0');
//     break;
//   }
//   case 0: {
//     console.log('number 0');
//     break;
//   }
//   case undefined: {
//     console.log('data === undefined');
//     break;
//   }
//   default: {
//     console.log('data is not available');
//   }
// }

//switch(a) {case a: {console.log("some message"}, default: {console.log("default message)}}
// =>
// if(a === a) {console.log("some message"} else{ console.log("default message) }
//
// let a = 2 + 2;
//
// let promptResult = +prompt("enter the number"); //+prompt("enter the number");
//
//
// console.log("Тип промпта", typeof promptResult);
// console.log("Значение промпта", promptResult);
//
// if(!isNaN(promptResult)) { //Является ли наш promptResult числом
//   switch (promptResult) {
//     case 3:
//       alert('Маловато');
//       break;
//     case 4:
//       alert('В точку!');
//       break;
//     case 5:
//       alert('Перебор');
//       break;
//     default:
//       alert('Нет таких значений');
//   }
// }
//
// while (isTrue) {
//   console.log("Wuhuuuuu");
//   isTrue = false;
// }

//isFalse = false
//1 iteration console.log(); isFalse = true;
// while(isFalse) console.log(), isFalse = false, while => X
// do {
//   console.log("Wuhuuuuu2");
//   console.log(isFalse);
//   isFalse = !isFalse;
// } while (isFalse);
//
// for(let i = 0; i < 5; i++){
//   console.log(1);
// }
//
// const user = {
//   name: "Artem",
//   age: 23,
//   id: "007",
//   city: "Sumy",
// }
//
//
// for (let key in user) {
//   console.log(key, user[key]);
// }
//
// console.log(Object.entries(user), "entries");
//
// for (let [key, value] of Object.entries(user)) {
//   console.log(key, value);
// }

function Sum (a, b) {
  if(typeof a === 'number' && typeof b === 'number') {
    return a + b;
  } else {
    return "Error";
  }
}

const counter = function (count) {
  console.log(arguments);
  for (let i = 0; i <= count; i++) {
    console.log(i);
  }
}

const secondCounter = (count) => {
};

console.log(secondCounter);

let count = 0;

function invoke(count) {
  console.log("Current value is ", count);

  if (count < 4) {
    invoke(count + 1);
  } else {
    console.log("Hello");
  }
}

//0, 1, 2, 3, 4, 5

console.log("Start");
invoke(count);
console.log("End");


// console.log(
//   typeof Sum(1, [1, 21, 3, 3])
// ); //3
// console.log(
//   Sum(1, [1, 21, 3, 3])
// ); //3
//
// console.log(1 + [1, 21, 3, 3]);
