// const main = document.querySelectorAll(".container");
//
// const main2 = document.getElementsByClassName("container");
//
// const title = document.querySelector("#title")
//

//
// const click2 = () => {
//   console.log("click2");
// }
//
// main.addEventListener("click", click);


// setTimeout() - функция которая сработает 1 раз, после какого-то интервала времени

// const click = () => {
//   alert("Welcome!")
// }
// const button = document.querySelector("button");

// const alertWithMessage = (message) => {
//   alert(message);
// }

// setTimeout((message) => {
//   alert(message);
// }, 3000, "Alert");
//


// const newMessage = "My name is. What?";
// setTimeout(click, 10000);
// setTimeout(alertWithMessage, 10000, "My name is. What?");
// setTimeout(alertWithMessage, 10000, newMessage);


// button.addEventListener("click", () => setTimeout(alertWithMessage, 5000, newMessage));

// setInterval()

// const counter = document.querySelector("#text");
//
// let count = 0;

// setInterval(() => {
//   count++;
//   counter.innerHTML = `${count}`;
// }, 1000);



/** Написать программу для напоминаний.
 *
 * Все модальные окна реализовать через alert.
 *
 * Условия:
 * - Если пользователь не ввёл сообщение для напоминания — вывести alert с сообщением «Ведите текст напоминания.»;
 * - Если пользователь не ввёл значение секунд,через сколько нужно вывести напоминание —
 *   вывести alert с сообщением «Время задержки должно быть больше одной секунды.»;
 * - Если все данные введены верно, при клике по кнопке «Напомнить» необходимо её блокировать так,
 *   чтобы повторный клик стал возможен после полного завершения текущего напоминания;
 * - После этого вернуть изначальные значения обоих полей;
 * - Создавать напоминание, если внутри одного из двух элементов input нажать клавишу Enter;
 * - После загрузки страницы установить фокус в текстовый input.
 */

// const inputMessage = document.querySelector("#reminder");
//
// const inputNumber = document.querySelector("#seconds");
//
// const button = document.querySelector("button");
//
// const reminder = () => {
//   if(inputMessage.value.trim().length === 0) {
//     alert("Ведите текст напоминания.");
//   }
//
//   if(+inputNumber.value < 1) {
//     alert("Время задержки должно быть больше одной секунды.");
//   }
//
//   button.disabled = true;
  // button.setAttribute("disabled", "");

//   setTimeout(() => {
//     alert(inputMessage.value);
//
//     inputMessage.value = "";
//     inputNumber.value = 0;
//     button.disabled = false;
//   }, inputNumber.value * 1000)
// }


// const reminder = () => {
//   if(inputMessage.value.trim().length === 0) {
//     alert("Ведите текст напоминания.");
//   } else if(+inputNumber.value < 1) {
//     alert("Время задержки должно быть больше одной секунды.");
//   } else {
//
//     button.disabled = true;
//     // button.setAttribute("disabled", "");
//
//     setTimeout(() => {
//       alert(inputMessage.value);
//
//       inputMessage.value = "";
//       inputNumber.value = 0;
//       button.disabled = false;
//     }, inputNumber.value * 1000)
//   }
// }

// const enterPress = (event) => {
//    event.key === "Enter" && reminder();
// }
//
// button.addEventListener("click", reminder);
//
// inputNumber.addEventListener("keypress", enterPress);
// inputMessage.addEventListener("keypress", enterPress);
//
// window.addEventListener("load", () => inputMessage.focus());


//Local storage - данные сохранятся даже если юзер покинет страницу.
//Session storage - существует во время сессии (пока юзер не покинет страницу).

//данные хранятся в формате -> ключ: значение -> key: value;
//И ключ и значение - строки

//localStorage.setItem(key, value);
// localStorage.setItem("5", "I am number 5");
// localStorage.setItem("13", "Legend number 13");
// localStorage.setItem("someKey", "Legend number1 1323");
// sessionStorage.setItem("5", "I am number 5");
// sessionStorage.setItem("13", "Legend number 13");
// sessionStorage.setItem("someKey", "Legend number1 1323");

// localStorage.key(index) -> Этот метод принимает в себя индекс пары в хранилище и возвращает её ключ;
// console.log(localStorage);
// console.log(localStorage.key(3));

//localStorage.getItem(key); -> обращаемся с помощью ключа -> получаем значение

// console.log(localStorage.getItem("5"));
// console.log(localStorage.getItem("someKey"));

//localeStorage.removeItem(key) => Удаляет элемент, ключ которого мы передали
// localStorage.removeItem("5");


//localeStorage.clear() => Удаляет все данные из хранилища
// sessionStorage.clear();

//How to set object in local/session storage

const teacher = {
  name: "Artem",
  age: {
    years: 23,
    month: 3
  },
}

const jsonTeacher = JSON.stringify(teacher);
console.log(JSON.parse(jsonTeacher));
console.log(JSON.stringify({name: "Artem"}));
localStorage.setItem("teacher", jsonTeacher)
