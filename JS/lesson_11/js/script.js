// const filterBy = (array, dataType) => {
//   let newArray = [];
//
//   array.forEach((element) => {
//
//   })
//
//   return newArray;//dataType - string  => [23, null];
// }
//
// // ['hello', 'world', 23, '23', null]
//
// filterBy(['hello', 'world', 23, '23', null], "string");
// filterBy(['hello', 'world', 23, '23', null, 'hello', 'world', 23, '23', null], "number");
// filterBy(['hello', 'world', 23, '23', null, 'hello', 'world', 23, '23', null], "null");
// filterBy(['hello', 'world', 23, '23', null, 'hello', 'world', 23, '23', null], "undefined");

const createDiv = document.createElement('div');
const header = document.getElementById('header');
// const newDiv = document.getElementById("divider");
const textNode = document.createTextNode('some text');
const title = document.getElementById('title');
const newTitle = document.createElement('h1');

createDiv.style.cssText = `
  background: red;
  width: 100px;
  height: 100px;
`;
//
// newDiv.style.cssText = `
//   background: green;
//   width: 100px;
//   height: 100px;
// `;

// document.body.append(createDiv);
// document.body.append(newDiv);
//document.body -> element к которому мы привязываем наш child
//append(div) -> div это child который мы вставляем

// document.body.prepend(createDiv);

// header.before(textNode);
// document.body.before(textNode);
// newDiv.after(textNode);

newTitle.innerText = 'Lesson 11';

newTitle.style.cssText = `
  font-size: 40px;
  color: red;
`;

title.replaceWith(newTitle);
console.log(newTitle);
//element.someFunction(child);
//element.append(child) -> вставляет наш елемент в конце дочерних элементов
//element.prepend(child) -> вставляет наш child в начале дочерних элементов
//element.before(child) -> вставляет наш child перед элементом к которому мы привязываем
//element.after(child) -> вставляет наш child после элемента которому мы привязываем
//element.replaceWith(child) -> заменяет наш элемент на child

//element.insertAdjacentHTML(place, HTMLstring);
// "beforebegin" | "afterbegin" | "beforeend" | "afterend"

header.style.cssText = `
  padding: 20px;
  border: 1px solid red;
`;

header.insertAdjacentHTML('beforebegin', '<h1> 1111111 </h1>');
header.insertAdjacentHTML('afterbegin', '<h1> 222222222 </h1>');
header.insertAdjacentHTML('beforeend', '<h1> 33333333333 </h1>');
header.insertAdjacentHTML('afterend', '<h1> 4444444444444 </h1>');

console.log(document.body.children);

//Что бы удалить, достаточно использовать .remove();
// document.body.children[2].remove();

/**
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Все стили для квадрата задать через JavaScript. style.cssText = ``;
 *
 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.  document.body.appendChild();

 */

// const squareSize = prompt("Enter square size");

const createSquare = (size) => {
  const square = document.createElement('div');

  square.style.cssText = `
    width: ${size}px;
    height: ${size}px;
    background-color: yellow;
  `;
  square.classList.add('square');

  document.body.appendChild(square);

  return square;
};

// const newSquare = createSquare(squareSize);

//Клонирование элемента -> cloneNode(false); cloneNode(true);

// console.log("header", header); //header == secondHeader

const newHeader = header.cloneNode(false);

// console.log("newHeader", newHeader); //header !== newHeader

const secondHeader = header.cloneNode(true);

// console.log("secondHeader", secondHeader);

//Document Fragment -> фрагмент документа -> обертка для html элементов страницы

let fragment = new DocumentFragment(); //<> </>


const peoples = [
  {name: "Artem", age: 23},
  {name: "Artem", age: 12},
  {name: "Artem", age: 2},
  {name: "Artem", age: 23},
  {name: "Artem", age: 23},
  {name: "Artem", age: 3},
  {name: "Artem", age: 23},
  {name: "Artem", age: 3},
  {name: "Artem", age: 23},
  {name: "Artem", age: 2443},
  {name: "Artem", age: 123},
  {name: "Artem", age: 23},
  {name: "Artem", age: 23},
  {name: "Artem", age: 23},
  {name: "Artem", age: 23},
  {name: "Artem", age: 23}
];

for (let i = 0; i < peoples.length; i++) {
  let p = document.createElement("p");
  let li = document.createElement("li");
  p.append(`${peoples[i].name}`);
  li.append(`${peoples[i].age}`);


  fragment.append(p);
  fragment.append(li);
}

console.log("before unpacking", fragment);

header.prepend(fragment);

console.log("after unpacking", fragment);
