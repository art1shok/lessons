// console.log(document);
//Выучить что такое DOM - Document object model -> объектная модель документа, представляющая всё содержимое страницы в виде объектов, которые можно изменять.


// console.log(document.getElementsByClassName('list-item'));
// console.log(document.getElementById('title'));

//
// Вывести в консоль элементы:
// 1) по id -> list;
// 2) по class => page-header__title;
// 3) по tag -> li;
// 4) ко второму элементу списка через css селекто (querySelector);
// 5) ко всему элементам списка через css селекто (querySelectorAll);

//
// console.log(document.getElementById('list'));
//
// const list = document.getElementById('list');
// Возвращает DOM елемент
// console.log(document.getElementsByClassName('page-header__title'));
//Коллекцию htmlDocument

// console.log(document.getElementsByTagName("li"));
//Коллекцию htmlDocument
// console.log(document.getElementsByTagName("li")[1]);

// console.log(document.querySelectorAll('li'));
// console.log(document.querySelector("li:nth-child(1)").style.backgroundColor = "yellow");




// console.log(document.querySelector("li:nth-child(2)"));
//Возвращает один DOM елемент
// console.log(document.querySelectorAll("li"));
// Возвращает коллекцию NodeList

// const element = document.querySelector('.page-header__title');
//
// console.log("class Name", element.className);
// console.log("class List", element.classList);
//
// //add - принимает в себя название класа, который мы добавляем елементу.
// console.log("class List", element.classList.add('title'));
// //remove - удаляет класс который передаем в качестве аргумента
// console.log("class List", element.classList.remove('title'));
// // replace - принимает в себя 2 аргумента, 1й - класс который хотим заменить
// // 2й - класс который мы вставляем на его место
// console.log("class List", element.classList.replace('page-header__title', "title"));
// //toggle - добавляет класс, если его нету, удаляет если есть
// console.log("class List", element.classList.toggle("title"));
// console.log("class List", element.classList.toggle("title"));

// innerHtml, outerHTML, textContent
//
// console.log(list.innerHTML); //в виде строки
// console.log(list.innerText); //в виде строки
// console.log(list.outerHTML); //innerHTML + element
// console.log(list.textContent); //
//
// На экране указан список товаров с указанием названия и количества на складе.
// *
// * Найти товары, которые закончились и:
//   * - Изменить 0 на «закончился»;
// * - Изменить цвет текста на красный;
// * - Изменить жирность текста на 600.
// *
// * Требования:
// * - Цвет элемента изменить посредством модификации атрибута style.
//
// const itemsList = document.querySelectorAll("li");
//
// console.log(itemsList);

// itemsList.forEach( function (item) {
//   if(item.innerText.includes(": 0")) {
//     item.innerText = item.innerText.replace(": 0", ": закончился");
//     item.style.color = "red";
//     item.style.fontWeight = "600";
//   }
// })
//
// itemsList.forEach( item => {
//   if(item.innerText.includes(": 0")) {
//     item.innerText = item.innerText.replace(": 0", ": закончился");
//     item.style.color = "red";
//     item.style.fontWeight = "600";
//   }
// });
// * Получить элемент с классом .remove.
// * Удалить его из разметки.
// *
// * Получить элемент с классом .bigger.
// * Заменить ему CSS-класс .bigger на CSS-класс .active.

let item1 = document.querySelectorAll(".remove");
// let item2 = document.querySelector(".remove");
// item2.classList.remove(".remove");
console.log(item1)

item1.forEach((element, idx) => {
  console.log(idx, element.classList)
  element.classList.remove('remove');
});
console.log(item1)




