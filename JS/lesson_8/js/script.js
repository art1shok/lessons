// const newUser = {
//   _name: "Artem",
//   _lastName: "Kvitka",
//   age: 23,
//   set (newName) {
//     this.name = newName;
//   }
// }
//
// console.log("Name", newUser._name) //
//
// console.log(newUser.name = "Vasiliy")
//
// console.log(newUser)
//
/**
 * Задание 1.
 *
 * Написать функцию, которая возвращает название дня недели (словом),
 * который был указанное количество дней назад.
 *
 * Функция обладает одним параметром:
 * - Число, которое описывает на сколько дней назад мы хотим вернуться, чтобы узнать желаемый день.
 *
 * Условия:
 * - Использование объекта Date обязательно.
 *
 * Заметка:
 * - Можно использовать готовый объект с днями недели.
 */
//
// const days = {
//   0: 'Воскресенье',
//   1: 'Понедельник',
//   2: 'Вторник',
//   3: 'Среда',
//   4: 'Thursday',
//   5: 'Friday',
//   6: 'Saturday',
// };
//
// const getDayAgo = (daysAgo) => {
//   const now = new Date();
//   console.log(now)
//
//   now.setDate(now.getDate() - daysAgo)
//
//   console.log(now)
//   return days[now.getDay()]
// }

// console.log(getDayAgo(50))

// function declarationSum (firstNumber) {
//   return firstNumber + 7;
// }
//
// const expressionSum = function (firstNumber, secondNumber) {
//   console.log(arguments);
//   return firstNumber + secondNumber;
// };
//
// declarationSum(1, 3);
// expressionSum(1, 3);
//
// const arrowSum = (firstNumber, secondNumber) => firstNumber + secondNumber;
// arrowSum(5);

const arrowOperation = firstNumber => (firstNumber + 7);
//
// const createNewUser = () => {
//   const newObject = {
//     name: prompt('Enter your first name'),
//     lastName: prompt('Enter your last name'),
//   }
//   console.log("inside create new user ", this);
//
//   return newObject;
// }
// const createNewUserExpression = function () {
//   const newObject = {
//     name: prompt('Enter your first name'),
//     lastName: prompt('Enter your last name'),
//   }
//   console.log("inside create new user expression ", this);
//
//   return newObject;
// }
// //
// // createNewUser();
// // createNewUserExpression();
//  // Когда вы используете многострочную стрелочную функцию
// const createNewUser2 = () => {
//   return ({
//     name: prompt('Enter your first name'),
//     lastName: prompt('Enter your last name'),
//   });
// };
//
// const createNewUser3 = () => ({
//   name: prompt('Enter your first name'),
//   lastName: prompt('Enter your last name'),
// });

// console.log(createNewUser());

//
// const cafe = {
//   name: "Blinochnaya",
//   menu: ["tea", "coffee"],
//   addMenuItem: function (newMenuItem) {
//     console.log("context of function", this);
//
//     const checkOrder = () => console.log("arrow function", this);
//
//     checkOrder();
//
//
//     const checkOrder2 = function () {
//       console.log("function expression",this)
//     }
//
//     checkOrder2();
//   },
// }
//
// cafe.addMenuItem("blinchiki");

const sum = (a, b) => a + b;
const difference = (a, b) => a - b;
const multiplication = (a, b) => a * b;
const divide = (a, b) => a / b;

// У нас есть сообщение которое нужно вывести в консоль, n-ое колличество раз.
//Нужно написать стрелочную функцию, которая будет принимать в качестве аргументов это сообщение
// и колличество его выведений в консоль.

// const logMessage =
//
// const message = prompt("Enter your message");
// const count = 2;
//
// const logMessage = (string, repeats) => {
//   for(let i = 0; i < repeats; i++) {
//     console.log(string)
//   }
// }
//
// logMessage(message, count);//10 times shows our message
//
//
// function showRest(...rest) {
//   console.log(arguments);
//   console.log(arguments.length);
//   console.log("rest", ...rest);
//
//   if(typeof rest[0] === 'number') {
//
//   }
//   return {
//     rest
//   }
// }
//
// showRest(21, "28.11.2022", 12, ["elementow"], {});

// console.log(newTeacher) // name: "Artem", id: 1, age: 23
// const prop = "id";
// const idData = 30;
// const someTeacher = {
//   [prop]: idData, // id: 30
// };

// console.log(someTeacher)//id: 30

const oldStudents = ['Ihor', 'Anya', 'Alexandr'];
const newStudents = ['Ihor', 'Natasha', 'Dima'];

const currentGroup = [...oldStudents, ...newStudents]; // деструктуризировать то есть разбивать массив или объект на элементы

// console.log(currentGroup);

const teacher = { name: 'Artem', id: 1 };
const teacherData = { age: 23, position: 'Frontend developer' };

const newTeacher = { ...teacher, ...teacherData, newKey: 'keyData' };

const deleteId = ({ id, ...rest }) => rest;

// const teacherWithoutId = deleteId(newTeacher);

// console.log(teacherWithoutId);//
// console.log(newTeacher);

const removeProperty = (prop) => ({ [prop]: _, ...rest }) => rest;
const removeId = removeProperty('id');

// console.log(removeId(newTeacher));

// ["apple", "banana", "milk"] .length -> 3
const object = {
  name: 'apple',
  age: 2,
  zaychiki: ['zaychenya1', 'zaychenya2'],
};

// console.log(object)
const productsBasket = [
  'apple', //0
  'banana', //1
  'milk', //2
  1,//3
  2,//4
  3,//5
  4,//6
  5,//7
  { string: 'VIshel zaychik pogulyat' },//8
  ['zaychenya1', 'zaychenya2']//9
];

console.log(Array.isArray([])); //true

//
// console.log(productsBasket.length) //10
// console.log(productsBasket) //
// console.log('Object');
//
// for (let key in object ) {
//   console.log(key, " : ", object[key]);
// } // for in для объектов

// console.log('Array');
//
// for (let element of productsBasket) {
//   console.log(element)
// } // for of для массивов

// Методы массивов
//Методы перебора массива

//ForEach => позволяет запускать определенную функцию для каждого елемента массива

const groupList = ['Dmytro', 'Natalia', 'Andrii', 'IhorK', 'Pasha'];

// groupList.forEach(function (item, index, array) {
//   console.log(`${item} has index ${index} in array ${array}`);
// });
//
// groupList.forEach((item, index, array) => {
//   console.log(`${item} has index ${index} in array ${array}`);
// });



//Добавление и удаление элементов в массиве

//push - добавляет элемент в конец массива

groupList.push("IhorM");

console.log(groupList); //'Dmytro', 'Natalia', 'Andrii', 'IhorK', 'Pasha', 'IhorM'

//pop достает элемент с конца массива

const lastElement = groupList.pop();

console.log(" ", groupList); //'Dmytro', 'Natalia', 'Andrii', 'IhorK', 'Pasha'
console.log(" ", lastElement); // "IhorM"

//shift/unshift
//shift достает элемент с начала массива

const firstElement = groupList.shift();


console.log(" ", groupList); //'Natalia', 'Andrii', 'IhorK', 'Pasha'
console.log(" ", firstElement); // "Dmytro"

//unshift добавляет элемент в начало массива

groupList.unshift("Dmytro");

console.log(groupList) //'Dmytro', 'Natalia', 'Andrii', 'IhorK', 'Pasha'

//Превращение массива
//map

const studentsCharacteristic = groupList.map((student, index, array) => {
  return student + " is perfect student";
});

console.log(studentsCharacteristic);

console.log(groupList);

const string = "s fse fsefs sfse";

console.log(string.split('s')); // [' f' , 'e', 'f', 'ef', ' ', 'f', 'e'];

console.log(string.split(" ") );// [ 's', 'fse', 'fsefs', 'sfse'];

const numberList = [ 1, 4, 2, 15, 7, 4];
                  // 0, 1, 2 , 3, 4, 5
console.log(numberList.sort()); //Числа превращаются в строки и затем сортируются;

console.log(numberList.sort((a, b) => b - a));

//reverse - переворачивает массив (последние элементы становятся первыми и наоборот)
console.log(numberList.sort((a, b) => b - a).reverse());

//Search in array

//indexOf
const newNumberList = [ ...numberList, 1, 2, 3, 5,12, 234, 2];


console.log(newNumberList)
console.log(newNumberList.indexOf(3)); //8

//lastIndexOf
console.log(newNumberList.indexOf(2)); //8

console.log(newNumberList.lastIndexOf(2)) //12


//includes

console.log(newNumberList.includes(8)); // false

console.log(newNumberList.includes(234)); // true


//filter
let newArray = [];

for (let element of newNumberList)  {
  if(element > 7) {
    newArray.push(element);
  }
}


const newNumberArray = newNumberList.filter((n) => n > 7);


console.log(newNumberArray);
console.log(newArray);



