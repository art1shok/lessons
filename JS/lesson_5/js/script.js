"use strict";
// let count = 0;
//
// let newArray = [4, 1, 2, 4, 12, 13, 3, 4, 5, 6, 12];
//   count =    0, 1, 2, 3, 4, 5, 6, 7,  8;
//
// let someString = "abrakadabra";

// while (count < newArray.length && count === newArray[count]) {
//   console.log(newArray[count], newArray);
//   count++;
// }

//
// do {
//   console.log("current Array element", newArray[count]);
//   count++;
// } while (count < newArray.length && count === newArray[count])
//
// do {
//   console.log("current element is ", someString[count], count);
//   count++;
// } while (count < someString.length)
//
// console.log(someString.split(''));
// console.log(newArray);
//
// for(let i = 0; i < newArray.length; ){
//     i++;
//     console.log(newArray[i] + 5);
// }

//
// function someFunction () {
//   let firstNumber = 1;
//
//
// }
//
// const anotherFunction = function () {
//
//
//   console.log(someFunction());
// }
//
// anotherFunction(); //

// !0 = 1;
// !5 = 1 * 2 * 3 * 4 * 5;
// !5 = 5 * 4 * 3 * 2 * 1;

// function factorialCalculating (number) {
//   console.log(number)
//   if (number === 0) {
//     return 1;
//   } else {
//     return number * factorialCalculating(number - 1);
//   }
// }
//
// console.log(factorialCalculating(5));

//Выделяется какое-то место в памяти, и вы берете это место;
// let teacherName = "Artem";
// let number = 1;
// console.log(number, "first");
//
// let secondNumber = number;
//
// console.log(secondNumber, "second");
//
// secondNumber = 2;
//
// console.log(secondNumber, "second");
// console.log(number, "first");
//
// let students = ["Natalia", "Anna", "Dmitro", "Ihor", "Aleksandr"]

//Объект это единственный не примитивный тип данных
//Ссылочный тип данных, ссылочная структура данных

// const teacher = {
//   name: teacherName,
//   age: 23,
//   position: "lector",
//   students,
//   sayHi() {
//     console.log("Hello my students");
//     return 1;
//   },
//   updateProperty(propertyName, propertyValue) {
//     // this.hasOwnProperty(propertyName);
//     if(propertyName in this) {
//       // this[propertyName] => this.${propertyName}
//       // this[propertyName] => this.age if propertyName === "age"
//       this[propertyName] = propertyValue;
//     } else {
//       console.log(`Property ${propertyName} does not exist`);
//       console.log("Property " + propertyName + " does not exist");
//     }
//   }
// }
//
// teacher.hasOwnProperty("age");
//
// console.log(teacher.sayHi());
//
// let secondTeacher = {};
// let secondTeacher = teacher;

// console.log(secondTeacher, "secondTeacher")


// console.log(secondTeacher, "secondTeacher");

// console.log(teacher, "teacher");

// Object.assign(secondTeacher, teacher);

// secondTeacher.name = "Roman";
// secondTeacher.age = 37;
//
// teacher.updateProperty("age", 37)
//
// console.log(teacher, "teacher");
//
// const student = {
//   name: "Vasuliy",
//   age: 34,
// }
//
// console.log(student);
//
// Object.freeze(student);
//
// student.age = 35;
// student.work ="IT";
// delete student.name;


// const secondStudent = {
//   name: "Olya",
//   age: 27,
// }
//
// Object.seal(secondStudent);
//
// secondStudent.work = "IT";

//
// function showStudent () {
//   document.open();
//   // document.write(student.age)
//   // document.write(student.name)
//   // document.write(student.work)
//   document.write(secondStudent.age)
//   document.write(secondStudent.name)
//
//
//   document.close();
// }

// Object.seal();
// Object.preventExtensions();

//
// const createUser = function (firstName, secondName, password, email) {
//   console.log(firstName, email)
//
//   const user = {
//     firstName: firstName.trim(),
//     secondName,
//     password,
//     email: email.toLowerCase(),
//   }
//
//   return user;
// }
//
// const firstUser = createUser("     Artem     ", "Kvitka", "qwerty", "SomeEmail@gmail.com")
//
// console.log(firstUser, "first User");


let newUser = {
  firstName: "Yuriy",
  lastName: "Gagarin"
};

Object.freeze(newUser)

newUser.firstName = "Sanyok";
console.log(newUser);


