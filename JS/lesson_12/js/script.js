// Написать небольшой список задач (to do list)

// Спрашивать у пользователя пункты для добавления в список до тех пор, пока
// он не нажмет отмена. Каждый текст введенный в prompt это новый элемент списка.
// Все пункты нужно вставить на страницу.
// Не забываем про семантику (список должен быть оформлен через ul);

//optional
// По клику на элемент списка - удалить этот пункт.
// Подсказка
// // elem.onclick = function () {
//     пишем что происходит по клику
//   };
//
// const deleteElement = (element) => {
//   element.remove();
// };
//
// const createToDoList = () => {
//   let task = prompt('Что нужно сделать?');
//
//   const main = document.getElementsByTagName('main')[0];
//   const ul = document.createElement('ul');
//
//   main.append(ul);
//   console.log('task', task);
//
//   while (task !== null) {
//     let li = document.createElement('li');
//     li.innerText = task;
//
//     // li.onclick = () => {
//     //   li.remove();
//     // }
//     li.onclick = () => deleteElement(li);
//
//     ul.append(li);
//     task = prompt('Что нужно сделать?');
//   }
// };

// createToDoList();

/** Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 *
 * */

//
// const LIGHT_CELL = '#ffffff';
// const DARK_CELL = '#161619';
// const CELLS = 8;
//
// const board = document.createElement('section');
//
// board.classList.add('board');
//
// const main = document.getElementsByTagName('main')[0];
//
// main.append(board);

// const fillChessBoard = (cell) => {
//   let rowIsEven = true;
//
//   for (let i = 1; i <= cell * cell; i += 1) {
//     let div = document.createElement('div');
//
//     if (i % 2 !== 0) {
//       div.style.backgroundColor = rowIsEven ? LIGHT_CELL : DARK_CELL;
//     } else {
//       div.style.backgroundColor = rowIsEven ? DARK_CELL : LIGHT_CELL;
//     }

//     if (i % 8 === 0) {
//       rowIsEven = !rowIsEven;
//     }
//
//     board.append(div);
//   }
// };

// fillChessBoard(CELLS);
//
// const fillChessBoard = (cell) => {
//   for (let i = 0; i < cell; i++) {
//     for (let j = 0; j < cell; j++) {
//       let div = document.createElement('div');
//       div.classList.add('cell');
//
//       if ((i + j) % 2 !== 0) {
//         div.style.backgroundColor = DARK_CELL;
//       } else {
//         div.style.backgroundColor = LIGHT_CELL;
//       }
//
//       board.append(div);
//     }
//   }
// };
//
// fillChessBoard(CELLS);
//
// // Сделать функцию светофор, которая "зажигает" красный, желтый,
// // зеленый цвет в html-разметке в зависимости от переданного аргумента (выбранный цвет)
// // Все остальные будут серого цвета.
//
//
// const chooseColor = (color) => {
//
//
// }
//
// chooseColor("red");
// // chooseColor("green");
// // chooseColor("yellow");
