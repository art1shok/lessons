// const ul = document.createElement("ul");
//
// document.body.append(ul);
//
// const showList = (array, parentElement) => {
//
//
// }
//
//
// showList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], ul)


const element = document.querySelector(".keypad");
// const element2 = document.getElementById("keypadEvent");


console.log(element);
//keydown - когда мы нажали на клавишу
//keyup - когда мы отпустили клавишу
//keypress - считается устаревшим, поэтому оно не используется

// window.addEventListener('keydown', (event) => {
//   console.log("onkeydown code", event.code); //Позволяет получить код клавиши.
//   console.log("onkeydown key", event.key); //Получить определенный символ
// })

// window.addEventListener('keyup', (event) => {
//   console.log("keyup", event.code);
//   console.log("keyup", event.key);
// })

/**
 * Создайте div с текстом "нажмите любую кнопку" (див привязать к body)
 *
 * При нажатии клавиши что бы текст внутри div менялся на " Нажатая клавиша: имя_нажатой_клавиши"
 *
 *
 */


// const div = document.createElement("div");
//
// div.innerText = "Нажмите любую клавишу!";
//
// document.body.append(div);

// window.addEventListener("keydown", (event) => {
//   div.innerText = ` Нажатая клавиша: ${event.key}`;
// })

/**
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 */

let size = 16;

window.addEventListener("keyup", (event) => {
  console.log(event.key)
  if(event.shiftKey && event.key === "+") {
    if(size < 30) {
      size++;
      document.body.style.fontSize = `${size}px`;
    }
  } else if (event.shiftKey && event.key === "-") {
    if(size > 10) {
      size--;
      document.body.style.fontSize = `${size}px`;
    }
  }
});


// window.addEventListener('scroll', function() {
//   document.getElementById('showScroll').innerHTML = pageYOffset + 'px';
// });

// const form = document.getElementsByTagName("form");
//
// form.addEventListener("submit", (event) => {
//
//
//   console.log(event);
// })

const input = document.querySelector("#input");
// console.log(input.value)
// console.log(input)
// input.onfocus = () => {
//   console.log("focus")
//   input.style.cssText = `
//     color: red;
//     font-size: 20px;
//     padding: 35px;
//
//     transition: 0.5s;
//   `
// }

//
// input.onblur = () => {
//   console.log(blur)
//   input.style.cssText = `
//     color: black;
//     font-size: 24px;
//     padding: 0;
//
//     transition: 0.5s;
//   `
// }


// input.addEventListener("change", (event) => {
//   console.log("Onchange is alive");
//   console.log(event);
//
// })
//
// input.addEventListener("input", (event) => {
//   console.log("Input is alive");
//   console.log(event);
// })
// input.addEventListener("cut", (event) => {
//   console.log("cut is alive");
//   console.log(event);
//
// })
input.addEventListener("copy", (event) => {
  const div = document.createElement("div");

div.innerText = "Нажмите любую клавишу!";

document.body.append(div);

})
input.addEventListener("paste", (event) => {
  const div = document.createElement("div");

  div.innerText = "New text";

  document.body.append(div);

})
window.addEventListener("copy", event => {
  event.preventDefault();
})
