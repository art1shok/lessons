const myVariable = () => 2;

const createUser = () => {
  const myVariable = () => 1;
  const myVariable2 = () => 1;
  const myVariable3 = () => 1;
  const myVariable4 = () => 1;
  const myVariable5 = () => 1;
  const myVariable6 = () => 1;
  const myVariable7 = () => 1;
  const myVariable8 = () => 1;
  const myVariable9 = () => 1;
  const newString = '123';
  const newString2 = '123';
  const newString3 = '123';
  const newString4 = '123';
  const newString5 = '123';
  const newString6 = '123';
  const newString7 = '123';
  const newString8 = '123';
};

//* Дан массив с днями недели.

//* Вывести в консоль все дни недели
//const days = [
// 'Monday',
//'Tuesday',
//'Wednesday',
////'Thursday',
// 'Friday',
//'Saturday',
//'Sunday',
//];
//С помощью обычного цикла for
//С помощью for … of
//С помощью forEach

const days = [
  'Monday', //1 iteration
  'Tuesday', //2 iteration
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

//
// for (let i = 0; i < days.length; i++) {
//   console.log("Our array element is", days[i]);
// }
//
// for (let element of days) {
//   console.log("Our array element is", element);
// }
// //value: string, index: number, array: string[]
// days.forEach((element) => console.log("Our array element is", element));
//

/*
* Написать мульти-язычную программу-органайзер.
*
* Программа спрашивает у пользователя на каком языке он желает увидеть список дней недели.
* После ввода пользователем желаемого языка программа выводит в консоль дни недели на указанном языке.
*
* Доступные языки (локали): ua, ru, en.
*
* Если введённая пользователем локаль не совпадает с доступными — программа переспрашивает его до тех пор,
* пока доступная локаль не будет введена.
*
* Условия:
* - Решение должно быть коротким, лаконичным и универсальным;
* - Всячески использовать методы массива;
* - Использование метода Object.keys() обязательно.
*/

const days2 = {
  ua: [
    'Понеділок',
    'Вівторок',
    'Середа',
    'Четвер',
    'П’ятниця',
    'Субота',
    'Неділя',
  ],
  ru: [
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
    'Воскресенье',
  ],
  en: [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ],
};

// const availableLanguages = ["ua", "ru", "en"];
//
// const availableLanguages = Object.keys(days2);
//
// console.log(availableLanguages)
// let chosenLocale = prompt(`Введите желаемую локаль, доступные локали: ${availableLanguages}`, "ua");
//
// // while(chosenLocale !== "en" || chosenLocale !== "ru" || chosenLocale !== "ua"){//somecondition
// //
// // }
//
// while(!availableLanguages.includes(chosenLocale)){ //Наш массив доступных языков не включает в себя выбраный юзером язык
//   chosenLocale = prompt(`Вы ввели неправильную локаль, попробуйте ещё раз. Доступные локали: ${availableLanguages}`, "ua");
// }
//
// days2[chosenLocale].forEach((value) => console.log(value));
// //объект days2, мы достаем значение из переменной chosenLocale  -> "en",
// //                                                 days2.en.forEach()

/**
 *
 * Написать функцию-помощник кладовщика replaceItems.
 *
 * Функция должна заменять указанный товар новыми товарами.
 *
 * Функция обладает двумя параметрами:
 * - Имя товара, который необходимо удалить;
 * - Список товаров, которыми необходимо заменить удалённый товар.
 *
 * Функция не обладает возвращаемым значением.
 *
 * Условия:
 * - Генерировать ошибку, если имя товара для удаления не присутствует в массиве;
 * - Генерировать ошибку, список товаров для замены удалённого товара не является массивом.
 *
 * Заметки:
 * - Дано «склад товаров» в виде массива с товарами через
 */

let storage = [
  'apple',
  'water',
  'banana',
  'pineapple',
  'tea',
  'cheese',
  'coffee',
];

// storage.splice

const replaceItems = (itemToDelete, arrayToInsert) => {
  if (!storage.includes(itemToDelete)) {
    console.log('Item is not exist');
  } else if (!Array.isArray(arrayToInsert)) {
    console.log('Input data is not array');
  } else {
    const itemToDeleteIndex = storage.indexOf(itemToDelete);
    storage.splice(itemToDeleteIndex, 1, ...arrayToInsert); //'apple',    'water',    'banana','pineapple', 'tea', "water", "salt"
  }
};

replaceItems('tree', ['pencil', 'salt']);

console.log(storage);

/**
 *
 * Написать функцию-исследователь навыков разработчика developerSkillInspector.
 *
 * Функция не обладает аргументами и не возвращает значение.
 * Функция опрашивает пользователя до тех пор, пока не введёт хотя-бы один свой навык.
 *
 * Если пользователь кликает по кнопке «Отменить» в диалоговом окне, программа оканчивает
 * опрос и выводит введённые пользователем навыки в консоль.
 *
 *
 * Условия:
 * - Список навыков хранить в форме массива;
 * - В списке навыков можно хранить только строки длиной больше, чем один символ.
 */

// const developerSkillInspector = () => {
//   const skillsList = [];
  //skills = prompt ("Do you have any asills?");
  //   let skill = prompt('Please enter your skill');
  //   while (skill) {
  //     skill = prompt("Enter some digital skill:")
  //     skillsList.push(skill);
  //   }

  // do {
  //   if (skill.length >= 1) {
  //     console.log(skill);
  //     skillList.push(skill);
  //   }
  // } while (skillList.length === 0);
//   console.log(skillsList);
// };
//
// developerSkillInspector();

const developerSkillInspector = () => {
  const skillsList = [];
  let skill = null;

  while(skill !== null || skillsList.length === 0) {
    skill = prompt("Enter some digital skill:");


    if(typeof skill === "string" && skill.length !== 0){
      skillsList.push(skill)
    }
  }

  console.log(skillsList);
};
developerSkillInspector();
